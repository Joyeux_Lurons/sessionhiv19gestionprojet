# -*- coding: utf-8 -*-

import random

class Ferrailles():  
    def __init__ (self, posdoc): 
        self.x,self.y=posdoc

class Dalek():  
    def __init__ (self, posdoc): 
        self.x,self.y=posdoc
    def deplace(self,x,y): # Déplace le dalek selon les coordonées du doc 
        if (self.x < x):
            self.x += 1
        if (self.y < y):
            self.y += 1
        if (self.x > x):
            self.x -= 1
        if (self.y > y):
            self.y -= 1
        
    
    

class Docteur():  
    def __init__ (self, posdoc, parent):
        self.parent = parent 
        self.x,self.y=posdoc    # posdoc est une liste, je peux peux prendre une séquence qui a n éléments je peux les dépackter 
                                # les uns après les autres pour les attributs de Docteur
                                                            
    def jouercoup(self, rep):
        if rep=="t":
            self.teleporter()
        elif rep == "z":
            self.zapper()
        else:
            self.deplacer(rep)
            
    def deplacer(self, rep):
        # to do:  Testerles cas frontières
        self.x+=rep[0]
        self.y+=rep[1]
        
        
    def zapper(self):
        print("Je Zappe")
    def teleporter(self):
        print("Je TÉLÉPORTE")
        

class Partie():
    nbrdalekparnivo=5
    largeur=8
    hauteur=6
    
    def __init__ (self): # En python mettre tous les attributs dans __init__ quitte à les déclarer comme None au début
        self.score=0
        self.niveaucourant=0
        self.doc=None
        self.daleks=[]
        self.ferrailles=[]
        self.creerprochainniveau()
        
    def creerprochainniveau(self):
        self.niveaucourant+=1
        nbdaleks=self.niveaucourant*Partie.nbrdalekparnivo + 1
        self.ferrailles=[] # permet de se débarasser de la référence à l'ancienne feraille pour qu'elle soit garbagée 
        listepos=[]
        while nbdaleks:
            x=random.randrange(Partie.largeur)
            y=random.randrange(Partie.hauteur)
            if[x,y] not in listepos:
                listepos.append([x,y])
                nbdaleks -=1
        posdoc=listepos.pop(0)
        self.doc=Docteur(posdoc,self)
        
        for i in listepos:
            self.daleks.append(Dalek(i))
    def jouercoup(self, rep):
        self.doc.jouercoup(rep)
        x=self.doc.x
        y=self.doc.y
        for i in self.daleks:
            i.deplace(x,y)
        # test- le doc a survécu 
        for i in self.daleks:
            if self.doc.x==i.x and self.doc.y==i.y:
                return self.score
        # test des collisions de daleks
        morts=[]
        for i in self.daleks:
            for j in self.daleks:
                if (i is not j) and i.x==j.x and i.y==j.y:
                    morts.append(i)
        
        for i in self.daleks:
            for j in self.ferrailles:
                 if i.x==j.x and i.y==j.y:
                    if i not in morts:
                        morts.append(i)               
        
        
        for i in morts:
            f=Ferrailles([i.x,i.y])
            self.ferrailles.append(f)
            self.daleks.remove(i)
            self.score+=5
            
        # test fin de niveau
        if len (self.daleks)==0: 
            self.creerprochainniveau()
            
        return -1 
    
        
        
class JeuDaleks():
    def __init__ (self):
        self.partiecourante=None
    
    def creerpartie(self):
        self.partiecourante=Partie()
    
    def jouercoup(self,rep):
        if rep=="f":
            rep=self.partiecourante.score
            self.partiecourante=None
        else:
            rep=self.partiecourante.jouercoup(rep)
        return rep

if __name__ == '__main__':
    j=JeuDaleks()