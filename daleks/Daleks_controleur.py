# -*- coding: utf-8 -*-

import Daleks_modele
import Daleks_vue
import Daleks_gestiondonnees

class Controleur():
    def __init__(self):
        self.modele=Daleks_modele.JeuDaleks()
        self.vue=Daleks_vue.Vue(self)
        self.gestdon=Daleks_gestiondonnees.Gestiondonnees()
        rep=self.vue.affichemenuinitial()
        self.fairechoixinitial(rep)
    
    def fairechoixinitial(self,rep):
        choix={"q":self.quitter,
               "s":self.trouvertafficherscore,
               "n":self.creerpartie}
        fnc=choix[rep]
        fnc() #python va lire le nom de la fonction et va y ajouter les parenthèses
    
    def fairechoixcoup(self,rep):
        choix={"q":self.quitter,
               "t":self.trouvertafficherscore,
               "z":self.creerpartie}
        fnc=choix[rep]
        fnc() #python va lire le nom de la fonction et va y ajouter les parenthèses et l'       
    def quitter(self):
        print("quitter")
        
    def trouvertafficherscore(self):
        print("score")
        
    def creerpartie(self):
        self.modele.creerpartie()
        self.vue.affichepartie(self.modele.partiecourante)
        rep=self.vue.affichemenujeu()
        self.jouercoup(rep)
    
    def jouercoup(self,rep):
        retourdejeu=self.modele.jouercoup(rep)
        if retourdejeu==-1:
            self.vue.affichepartie(self.modele.partiecourante)
            rep=self.vue.affichemenujeu()
            self.jouercoup(rep)
        else:
            print("GAME Over, folks -", retourdejeu)            
if __name__ == '__main__':
    c=Controleur()