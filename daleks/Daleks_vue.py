# -*- coding: utf-8 -*-

import os



class Vue(): #classe gérant la vue du jeu Daleks
    def __init__ (self, parent):
        self.parent=parent
        
    def clrscreen(self):
        for i in range(100):
            print(' ')
    
    def affichemenujeu(self):
        listereppossible=["f","t","z",
                          "1","2","3","4","5"
                          ,"6","7","8","9"]
        mappeur={ "1":[-1,1],"2":[0,1],
                 "3":[1,1],"4":[-1,0],"5":[0,0],"6":[1,0],
                 "7":[-1,-1],"8":[0,-1],"9":[1,-1]}
        print("Bonne partie")
        val=1
        while val:
            print("Choisissez votre option")
            rep = input("f=fin de partie,t=téléporter,z=zapper,clavier numérique pour déplacement du doc ")
            if rep in listereppossible:
                print(rep)
                if rep in mappeur.keys():
                    rep=mappeur[rep]
                val=0
        return rep
                   
    def affichemenuinitial(self):
        listereppossible=["q","n","s"]
        print("Bienvenue dans le jeu Daleks")
        val=1
        while val:
            print("Choisissez votre option")
            rep = input("q=quitter,n=Nouvelle partie,s=Score ")
            if rep in listereppossible:
                val=0
                
        return rep    
    
    
    def affichepartie(self, partie):
        self.clrscreen()
        matjeu=[]
        for i in range(partie.hauteur):
            ligne=[]
            for j in range(partie.largeur):
                ligne.append("-")
            matjeu.append(ligne)
        matjeu[partie.doc.y][partie.doc.x]="D"
        for i in partie.daleks:
            matjeu[i.y][i.x]="W"
            
        # 
        for i in partie.ferrailles:
            matjeu[i.y][i.x]="F"
        for i in matjeu:
            print(i)        
        
        
if __name__ == '__main__':
    j=Vue(0)
    j.affichemenuinitial()