# -*- coding: utf-8 -*-

import TD_modele
import TD_vue

class Controleur():
    def __init__(self):
        self.modele=TD_modele.TD_Partie(self)
        self.vue=TD_vue.Vue(self,self.modele)
        self.jouercoup()
        self.vue.root.mainloop()
        
    def jouercoup(self):
        rep=self.modele.jouercoup()
        self.vue.affichepartie()
        self.vue.root.after(50,self.jouercoup)
        
    def creertour(self,x,y):
        rep=self.modele.creertour(x,y)
        self.vue.affichertour(x,y)
        
if __name__ == '__main__':
    c=Controleur()