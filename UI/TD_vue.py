# -*- coding: utf-8 -*-
import random
from tkinter import *


class Vue():
    def __init__ (self, parent, modele):
        self.parent=parent
        self.mod=modele
        self.initpartie()
    
    
    
    def initpartie(self):
        self.root=Tk() ## Tk() est une fonction interne qui initialise l'engin graphique 
        self.canevas=Canvas(self.root,width=self.mod.largeur,
                            height=self.mod.hauteur,bg="red")
        self.canevas.pack()
        self.canevas.bind("<Button>", self.creertour) # Manière d'accéder aux événements dans tkinter
        
    def creertour(self, evt):
        self.parent.creertour(evt.x,evt.y)
        
    def affichertour(self,x,y):
        self.canevas.delete("tour")
        self.canevas.create_rectangle(x-10,y-40,x+10,y,fill="purple",tags=("passif","tour"))
        
        
    def affichepartie(self):
        self.canevas.delete("actif")
        for i in self.mod.creeps:
            self.canevas.create_oval(i.x-10,i.y-10,i.x+10,i.y+10,
                                    fill="yellow", tags=("actif", "creep"))
            
        for i in self.mod.tours:
            for j in i.projectiles:
                self.canevas.creat_oval(i.x-2,i.y-2,i.x+2,i.y+2,
                                    fill="lightblue", tags=("actif", "projectile"))
            
        
        
        
        
        
        
        
        
        
        
        