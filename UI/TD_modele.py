# -*- coding: utf-8 -*-
import random
import helper

class Tour():
    def __init__ (self, parent,x,y):
        self.parent=parent
        self.x=x
        self.y=y
        self.projectiles=[]


class Creep():
    def __init__ (self, parent):
        self.parent=parent
        self.x=0
        self.y=random.randrange(self.parent.hauteur)
        self.ciblex=self.parent.largeur
        self.cibley=random.randrange(self.parent.hauteur)
        self.mana=10
        self.vitesse=random.randrange(4)+2
        self.angle=helper.Helper.calcAngle(self.x,self.y,self.ciblex,self.cibley)
    
    def deplacer(self):
        self.x,self.y=helper.Helper.getAngledPoint(self.angle,self.vitesse,self.x,self.y)



class TD_Partie():
    def __init__ (self, parent, largeur=800, hauteur=600):
        self.parent=parent
        self.largeur=largeur
        self.hauteur=hauteur
        self.attente=0
        self.nbrmaxcreep=50
        self.creeps=[]
        self.tours=[]
        self.creerCreeps()
        
    def creerCreeps(self):
        if len(self.creeps)<50:
            if self.attente==0:
                c=Creep(self)
                self.creeps.append(c)
                self.attente=random.randrange(10)+5
            else:
                self.attente-=1
    
    def jouercoup(self):
        for i in self.creeps:
            i.deplacer()
        self.creerCreeps()
        
    def creertour(self,x,y):
        if len(self.tours)==0:
            t=Tour(self,x,y)
            self.tours.append(t)
        else:
            self.tours[0].x=x
            self.tours[0].y=y
        return 1
            
        
        
            
            